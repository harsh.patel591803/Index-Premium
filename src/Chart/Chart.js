import React from 'react'
import ReactApexChart from 'react-apexcharts';

const ChartComp = ({chartOptions, seriesArray, numCharts, selectedSymbols, index}) => {
    console.log('seriesArray : ', seriesArray);
    return(
        <ReactApexChart
            // key={new Date().toISOString()}
            options={chartOptions}
            series={seriesArray[index] ? [seriesArray[index]] : [{ name: 'Loading...', data: [] }]}
            type="line"
            height={350}
          />
    );
};

export default ChartComp